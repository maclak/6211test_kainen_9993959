﻿using System;
using System.Collections.Generic;


namespace Question1TestFile
{
    class Question1
    {
        /*------------------------------------------------------------------------------------------------------------------------*/
        /*--------------------------Question 1A - Fix the population of the List of unique 4 digit numbers------------------------*/
        /*------------------------------------------------------------------------------------------------------------------------*/

        private const int size = 100;
        public static List<int> NumsLst = new List<int>();
        public static void PopulateList()
        {
            Random rand = new Random();
            int min = 1;
            int max = 4;
            string num = 0;
            for (int i = 0; i < size; i++)
            {
                NumsLst.Add((min, max));
            }

        }


        /*----------------------------------------------------------------------------------------------------------------------------------*/
        /*-------- Question 1B - Fix converting the List to array and displaying the contents with the largest and smallest values ---------*/
        /*----------------------------------------------------------------------------------------------------------------------------------*/

        public void DisplayData(List<int> NumsLst, int size)
        {
            
            int[] arr = new int[size];
            arr = NumsLst.ToArray();
            int max = 0;
            int min = 0;

            for (int i = 1; i <= arr.Length; i++)
            {
                Console.Write(_arr[i - 1] + " ");
                if (i % 10 == 0)
                    Console.WriteLine();
            }
            Console.WriteLine();

            for (int i = 0; i < arr.Length; i++)
            {
                sum += arr[i];
                if (max < arr[i])
                {
                    max = arr[i];
                }
                if (min > arr[i])
                {
                    min = arr[i];
                }
            }
            Console.WriteLine("\n\t   The smallest value is: {0}", min);
            Console.WriteLine("\t   The largest value is: {0}", max);
            NumsLst.Clear();
        }

        /*------------------------------------------------------------------------------------------------------------------------*/
        /*---------------------------------------------------Question 1 END-------------------------------------------------------*/
        /*--------------------------------------- NO fixing is required below this point------------------------------------------*/
        /*------------------------------------------------------------------------------------------------------------------------*/






        static void Main(string[] args)
        {
            DisplayTitle(1); //calls method to display Title

            PopulateList();
            DisplayData();

            CompleteExit(); //calls method for exit routine
            Console.ReadLine(); //Readline will wait for an Enter press before closing

        }

        /*------------------------------------------------------------------------------------------------------------------------*/
        /*--------------------------- Static methods below are for assistance in displaying etc ----------------------------------*/
        /*---------------------------------------- MAKE NOT CHANGES TO THESE METHODS ---------------------------------------------*/
        /*------------------------------------------------------------------------------------------------------------------------*/

        public static void DisplayTitle(int num)//This method simply sets up and displays the Title for the question.  It receives the question number as a parameter
        {
            Console.SetWindowSize(51, 40);
            Console.BufferWidth = 51;
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(" *************************************************");
            Console.WriteLine(" **                                             **");
            Console.Write(" **");
            Console.ForegroundColor = ConsoleColor.White;
            Console.Write("                  Question{0}                  ", num);
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("**");
            Console.WriteLine(" **                                             **");
            Console.WriteLine(" *************************************************");
            Console.ForegroundColor = ConsoleColor.White;
        }

        public static void CompleteExit()//This method simply sets up and displays the exit routine
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(" *************************************************");
            Console.WriteLine(" **                                             **");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write(" **          Press ");
            Console.ForegroundColor = ConsoleColor.White;
            Console.Write("ENTER");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(" to exit the app.       **");
            Console.WriteLine(" **                                             **");
            Console.WriteLine(" *************************************************");
            Console.ForegroundColor = ConsoleColor.White;
        }
        public static double[] DubArray(int size)
        {
            Random rand = new Random();
            double min = 1.01;
            double max = 9.99;
            double[] arr = new double[size];

            for (int i = 0; i < size; i++)
            {
                arr[i] = Math.Round((rand.NextDouble() * (max - min) + min), 2);
            }
            return arr;
        }
    }
}
